import os
import random
import time
from typing import Literal

from celery import Celery
from celery.utils.log import get_task_logger
from redis import Redis

celery = Celery('tasks')
celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://redis:6379/0")
celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://redis:6379/0")
celery.conf.task_track_started = True

logger = get_task_logger(__name__)

rds = Redis(
    host="redis",
    port=6379
)


@celery.task(name="tasks.calculate", bind=True)
def calculate(self, x: float, y: float, sign: Literal["+", "-", "*", "/"]):
    # rds.set(f"tasks.calculate-{self.request.id}", 'STARTED')
    operation = f"{x} {sign} {y}"
    logger.info(f"{self.request.id}: Calculating: {operation}")

    time.sleep(random.randint(1, 10))  # simulate working process

    try:
        result = {'data': eval(operation)}
        # rds.set(f"tasks.calculate-{self.request.id}", 'SUCCESS')
    except ZeroDivisionError:
        # ! We could check this case at the endpoing level for this simple task, but exceptions should be covered here anyway
        result = {'error': 'Cannot divide by 0!'}
        # rds.set(f"tasks.calculate-{self.request.id}", 'FAILED')
    except Exception as e:
        result = {'error': f'Unhandled exception: {e}'}
        # rds.set(f"tasks.calculate-{self.request.id}", 'FAILED')

    logger.info(f"{self.request.id}: Result: {result}")
    return result
