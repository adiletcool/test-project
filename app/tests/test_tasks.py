import pytest

from worker import calculate


@pytest.mark.parametrize("x,y,sign,expected_key,expected_value", [
    (1, 1, '+', 'data', 2),
    (10, 5, '/', 'data', 2.0),
    (10, 0, '*', 'data', 0),
    (3, 0, '/', 'error', 'Cannot divide by 0!'),
    (0, 0, '/', 'error', 'Cannot divide by 0!')
])
def test_create_task(x, y, sign, expected_key, expected_value):
    assert calculate.run(x, y, sign).get(expected_key) == expected_value


def test_app_create_task():
    # ! sudo docker compose exec backend python -m pytest -k "test_app_create_task"

    from starlette.testclient import TestClient
    from main import app
    from redis import Redis
    import time

    client = TestClient(app)
    # should be development redis actually
    rds = Redis(
        host="redis",
        port=6379,
        decode_responses=True
    )
    rds.flushdb()
    rds.flushall()

    # ! Create a task
    post_resp = client.post(
        url="api/tasks/create",
        params={"x": 10, "y": 5, "sign": "/"},
    )
    created_task = post_resp.json()
    assert 'task_id' in created_task

    # ! Get status of created task
    result_resp = client.get(
        "api/tasks/result",
        params={"task_id": created_task['task_id']},
    )
    result_data = result_resp.json()
    assert 'task_status' in result_data
    assert result_data['task_status'] == 'STARTED'

    # ! Ensure task_id in /api/tasks/all
    all_tasks = client.get("api/tasks/all")
    all_tasks_data = all_tasks.json()
    assert len(all_tasks_data) == 1 and all_tasks_data[0]['task_id'] == created_task['task_id']

    time.sleep(5)

    # ! Ensure task was finished successfully
    result_resp = client.get(
        "api/tasks/result",
        params={"task_id": created_task['task_id']},
    )
    result_data = result_resp.json()
    assert result_data['task_status'] == 'SUCCESS'

    # ! Ensure task status was updated in api/tasks/all
    all_tasks = client.get("api/tasks/all")
    all_tasks_data = all_tasks.json()
    assert all_tasks_data[0]['task_status'] == 'SUCCESS'
