from fastapi import FastAPI
from routers import tasks

app = FastAPI(
    title="Тестовое задание Pro.Finansy",
    description="[Flower Dashboard](/flower/dashboard)"
)

app.include_router(tasks.router)
