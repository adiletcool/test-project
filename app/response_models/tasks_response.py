from typing import Optional

from pydantic import BaseModel


class CreateTaskResponse(BaseModel):
    task_id: str


class GetTaskResponse(BaseModel):
    task_status: str
    result: Optional[float] = None
    error: Optional[str] = None


class TaskStatusResponse(BaseModel):
    task_id: str
    task_status: str
