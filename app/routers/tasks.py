import json
from typing import Literal, List

from celery import current_app
from celery.result import AsyncResult
from fastapi import APIRouter
from redis import Redis

from response_models import CreateTaskResponse, GetTaskResponse, TaskStatusResponse
from worker import calculate

current_app.loader.import_default_modules()

router = APIRouter(
    prefix="/api/tasks",
    tags=["Tasks"]
)

rds = Redis(
    host="redis",
    port=6379,
    decode_responses=True
)


# rds.flushdb()
# rds.flushall()


@router.post('/create',
             response_model=CreateTaskResponse,
             description="Creates a task with provided parameters and returns created task id")
async def create_task(x: float, y: float, sign: Literal["+", "-", "*", "/"]):
    new_task = calculate.delay(x, y, sign)
    return CreateTaskResponse(task_id=new_task.id)


@router.get('/result',
            response_model=GetTaskResponse,
            description="""Receives task id generated by */api/tasks/create*. 
            Returns tasks status, task data (if no errors occured) and task error (if any error occured)""")
async def get_task_result(task_id: str):
    task = AsyncResult(task_id)
    task_status = task.status

    if task_status in ['PENDING', 'STARTED']:
        return GetTaskResponse(task_status=task_status)

    task_result = task.result.get('data')
    if task_result is not None:
        return GetTaskResponse(task_status=task_status, result=task_result)

    task_error = task.result.get('error')
    return GetTaskResponse(task_status=task_status, error=task_error)


@router.get('/all',
            response_model=List[TaskStatusResponse],
            description="Returns a list of all tasks IDs and statuses")
async def get_tasks_statuses():
    result = []

    # for key in rds.kets("tasks.calculate-*"):

    for key in rds.keys("celery-task-meta-*"):
        task_meta = json.loads(rds.get(key))
        task_id = str(key).split("celery-task-meta-")[-1]

        result.append({
            'task_id': task_id,
            'task_status': task_meta.get('status'),
            # 'result': task_meta.get('result'),
        })

    return result
