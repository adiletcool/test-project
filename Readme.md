## Test on server:
* Documentation: http://51.250.24.20 or http://51.250.24.20/docs
* Flower dashboard: http://51.250.24.20/flower/dashboard


Create task
```
curl -X 'POST' \
  'http://51.250.24.20/api/tasks/create?x=11&y=22&sign=%2B' \
  -H 'accept: application/json' \
  -d ''
```
Get task result
```
curl -X 'GET' \
  'http://51.250.24.20/api/tasks/result?task_id=<YOUR_TASK_ID>' \
  -H 'accept: application/json'
```
Get all tasks
```
curl -X 'GET' \
  'http://51.250.24.20/api/tasks/all' \
  -H 'accept: application/json'
```


## Test locally:
```
git clone https://gitlab.com/adiletcool/test-project.git
cd test-project/
```

```
docker compose up -d --build --scale worker=2
```

* Documentation: http://127.0.0.1 or http://127.0.0.1/docs
* Flower dashboard: http://127.0.0.1/flower/dashboard 


Create task
```
curl -X 'POST' \
  'http://127.0.0.1/api/tasks/create?x=123&y=123&sign=-' \
  -H 'accept: application/json' \
  -d ''
```

Get task result
```
curl -X 'GET' \
  'http://127.0.0.1/api/tasks/result?task_id=<YOUR_TASK_ID>' \
  -H 'accept: application/json'
```

Get all tasks
```
curl -X 'GET' \
  'http://127.0.0.1/api/tasks/all' \
  -H 'accept: application/json'
```